﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace post_grad_administrator
{
    /// <summary>
    /// Run program.
    /// (I know this is not a pretty looking class to make a simple console application.
    /// Please don't judge me..:P)
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Admin!");
            //SeedData();

            bool done = false;
            using (PostGradDbContext db = new PostGradDbContext())
            {
                SerializeDbToJSON(db);
                List<Student> students = db.Students.Include(s => s.Supervisor).ToList();
                List<Supervisor> supervisors = db.Supervisors.Include(s => s.SupervisorSubjects).ThenInclude(s => s.Subject).ToList();
                List<Subject> subjects = db.Subjects.ToList();
                int id;
                Student student = null;
                Supervisor supervisor = null;
                while (!done)
                {
                    Console.WriteLine(@"
Select option below and press enter:
i: insert supervisor in db
s: insert student in db
p: print students with supervisor
l: print supervisors
u: update student
e: add subject to supervisor
d: delete student
any other key to quit");

                    string option = Console.ReadLine();
                    Console.Clear();

                    supervisors = db.Supervisors.ToList();
                    students = db.Students.Include(s => s.Supervisor).ToList();
                    switch (option)
                    {
                        case "i":
                            Console.WriteLine("Insert firstname followed by the enter key:");
                            string firstname = Console.ReadLine();
                            Console.WriteLine("Insert lastname followed by the enter key:");
                            string lastname = Console.ReadLine();
                            db.Supervisors.Add(new Supervisor { Firstname = firstname, Lastname = lastname });

                            break;
                        case "p":
                            foreach (Student s in students) Console.WriteLine($"{s.DOB}\t{s.Firstname} {s.Lastname}\tSupervisor: {s.Supervisor.Firstname} {s.Supervisor.Lastname}");
                            break;
                        case "l":
                            foreach (Supervisor sup in supervisors) Console.WriteLine($"ID: {sup.Id}\t{sup.Firstname} {sup.Lastname}");
                            Console.WriteLine("Enter an id form above to print supervisors subjects.");
                            id = int.Parse(Console.ReadLine());
                            Supervisor super = supervisors.Find(super => super.Id == id);
                            Console.WriteLine("Following subject(s) are registered on {0} {1}:", super.Firstname, super.Lastname);
                            foreach (SupervisorSubject ss in super.SupervisorSubjects) Console.WriteLine(ss.Subject.Name);
                            break;
                        case "s":
                            int studentnr;
                            ReadStudentInput(supervisors, student, out studentnr);
                            db.Students.Add(student);
                            db.StudentNumbers.Add(new StudentNumber { StudentIdentificationNumber = studentnr, Student = student });
                            break;
                        case "u":
                            Console.WriteLine("Enter the ID of the student you want to edit:");
                            foreach (Student s in students) Console.WriteLine($"ID: {s.Id}\t{s.Firstname} {s.Lastname}");
                            id = int.Parse(Console.ReadLine());
                            student = students.Find(s => s.Id == id);
                            ReadStudentInput(supervisors, student, out studentnr);
                            break;
                        case "e":
                            foreach (Supervisor s in supervisors) Console.WriteLine($"ID: {s.Id}\t{s.Firstname} {s.Lastname}");
                            Console.WriteLine("Enter the ID of the supervisor you want to add subjects to.");
                            id = int.Parse(Console.ReadLine());
                            supervisor = supervisors.Find(s => s.Id == id);
                            foreach (Subject sub in subjects) Console.WriteLine($"ID: {sub.Id}\tName: {sub.Name}");
                            Console.WriteLine("Enter the ID of the subject you want to add.");
                            int subid = int.Parse(Console.ReadLine());
                            db.SupervisorSubjects.Add(new SupervisorSubject() { SubjectId = subid, SupervisorID = id });
                            break;
                        case "d":
                            Console.WriteLine("Enter the ID of the student you want to delete:");
                            foreach (Student s in students) Console.WriteLine($"ID: {s.Id}\t{s.Firstname} {s.Lastname}");
                            id = int.Parse(Console.ReadLine());
                            db.Students.Remove(students.FirstOrDefault(s => s.Id == id));
                            break;
                        default:
                            done = true;
                            break;
                    }
                    db.SaveChanges();
                }
            }

        }


        public static void SeedData()
        {
            using (PostGradDbContext dbContext = new PostGradDbContext())
            {
                Supervisor supervisor = new Supervisor() { Firstname = "Nick", Lastname = "Some Lastname" };
                dbContext.Supervisors.Add(supervisor);

                DateTime.TryParseExact("28/02/1999", "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out DateTime dt);
                Student student = new Student() { DOB = dt, Firstname = "Mari", Lastname = "T-J", Supervisor = supervisor };
                dbContext.Students.Add(student);
                dbContext.StudentNumbers.Add(new StudentNumber { StudentIdentificationNumber = 123456789, Student = student });

                Subject subject = new Subject() { Name = "C#", Description = "Course in C#" };
                dbContext.Subjects.Add(subject);

                // Insert id's for whats in the db. I know not pretty..
                /*                dbContext.SupervisorSubjects.Add(new SupervisorSubject() { SubjectId = 4, SupervisorID = 2 });
                */
                dbContext.SaveChanges();

            }
        }

        public static void ReadStudentInput(List<Supervisor> supervisors, Student student, out int studentnr)
        {
            studentnr = 0;
            Console.WriteLine("Enter firsname:");
            student.Firstname = Console.ReadLine();
            Console.WriteLine("Enter lastname:");
            student.Lastname = Console.ReadLine();
            Console.WriteLine("Enter date of birth in following format: dd/MM/yyyy");
            string date = Console.ReadLine();
            DateTime.TryParseExact(date, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out DateTime dt);
            student.DOB = dt;
            Console.WriteLine("Select one of the supervisors below for the new student using their id:");
            foreach (Supervisor s in supervisors) Console.WriteLine($"ID: {s.Id}\tName: {s.Firstname} {s.Lastname}");
            student.SupervisorId = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter a 9 digit studentnumber just to demonstrate one to many relationship:");
            studentnr = int.Parse(Console.ReadLine());
        }

        private static void SerializeDbToJSON(PostGradDbContext db)
        {
            List<Supervisor> linqedDb = db.Supervisors
            .Include(s => s.SupervisorSubjects)
                .ThenInclude(s => s.Subject)
            .Include(s => s.Students)
                .ThenInclude(s => s.StudentNumber)
            .ToList();

            string jsonString = JsonConvert.SerializeObject(
                linqedDb,
                Formatting.Indented,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }
                );
            // I don't have permition to write file to desired location so I just add the file to project folder
            //string dir = "C:\\Users\\mtjoha\\source\\repos\\assignments\\post-grad-administrator\\post-grad-administrator";
            File.WriteAllText("dbJSON.json", jsonString);
        }

    }
}
