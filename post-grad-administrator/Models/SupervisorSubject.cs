﻿using System;
using System.Collections.Generic;
using System.Text;

namespace post_grad_administrator
{
    /// <summary>
    /// Joining class between Supervisor and Subject as a supervisor can supervise for 
    /// multiple subjects and a subject can have multiple supervisors. This joining class
    /// contains both navigation and FK from both classes. Each class has a collection navigation
    /// property in their class.
    /// </summary>
    class SupervisorSubject
    {
        public int SupervisorID { get; set; }
        public Supervisor Supervisor { get; set; }
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }

    }
}
