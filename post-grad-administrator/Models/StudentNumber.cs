﻿using System;
using System.Collections.Generic;
using System.Text;

namespace post_grad_administrator
{
    /// <summary>
    /// FK from Student class because StudentNumber is dependent on the existance of
    /// a Student. A StudentNumber can not exist without a Student, so this is composition.
    /// </summary>
    class StudentNumber
    {
        public int Id { get; set; }
        public int StudentIdentificationNumber { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}
