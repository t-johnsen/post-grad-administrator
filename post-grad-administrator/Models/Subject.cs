﻿using System;
using System.Collections.Generic;
using System.Text;

namespace post_grad_administrator
{
    /// <summary>
    /// Many to many relationship between Subject and Supervisor. Therefor not a FK from
    /// Supervisor here. Has collection navigation property to joining class: SupervisorSubject.
    /// </summary>
    class Subject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<SupervisorSubject> SupervisorSubjects { get; set; }

    }
}
