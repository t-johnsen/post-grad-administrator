﻿using System;
using System.Collections.Generic;
using System.Text;

namespace post_grad_administrator
{
    /// <summary>
    /// Student class with one to many relationship with Supervisor. Student is dependant class and
    /// therefor contains both navigation property and a foreign key (FK) property.
    /// 
    /// One to one relationship with StudentNumber class which is dependant on Student by composition. Therefor
    /// only navigation property of StudentNumber here, not foreign key property.
    /// </summary>
    class Student
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
        public int SupervisorId { get; set; }
        public Supervisor Supervisor { get; set; }
        public StudentNumber StudentNumber { get; set; }
    }
}
