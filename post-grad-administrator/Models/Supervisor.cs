﻿using System;
using System.Collections.Generic;
using System.Text;

namespace post_grad_administrator
{
    /// <summary>
    /// Principle class of the Supervisor-Student relationship. FK from Supervisor stored in Student.
    /// 
    /// Many to many relationship between Subject and Supervisor. Therefor not a FK from
    /// Subject here. Has collection navigation to joining class: SupervisorSubject.
    /// </summary>
    class Supervisor
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public ICollection<Student> Students { get; set; }
        public ICollection<SupervisorSubject> SupervisorSubjects { get; set; }

    }
}
