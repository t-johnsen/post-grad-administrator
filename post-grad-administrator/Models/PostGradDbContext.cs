﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace post_grad_administrator
{
    class PostGradDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<StudentNumber> StudentNumbers { get; set; }
        public DbSet<SupervisorSubject> SupervisorSubjects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7375\\SQLEXPRESS;Initial Catalog=PostGradDB;Integrated Security=True");
        }

        // Configuring of composite key between Supervisor and Subject
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SupervisorSubject>().HasKey(ss => new { ss.SupervisorID, ss.SubjectId });
        }
    }
}
