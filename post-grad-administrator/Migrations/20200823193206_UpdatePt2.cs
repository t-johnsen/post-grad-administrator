﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace post_grad_administrator.Migrations
{
    public partial class UpdatePt2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Supervisors_SupervisorId",
                table: "Students");

            migrationBuilder.AlterColumn<int>(
                name: "SupervisorId",
                table: "Students",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "StudentNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentNumbers_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SupervisorSubjects",
                columns: table => new
                {
                    SupervisorID = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupervisorSubjects", x => new { x.SupervisorID, x.SubjectId });
                    table.ForeignKey(
                        name: "FK_SupervisorSubjects_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SupervisorSubjects_Supervisors_SupervisorID",
                        column: x => x.SupervisorID,
                        principalTable: "Supervisors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentNumbers_StudentId",
                table: "StudentNumbers",
                column: "StudentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SupervisorSubjects_SubjectId",
                table: "SupervisorSubjects",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Supervisors_SupervisorId",
                table: "Students",
                column: "SupervisorId",
                principalTable: "Supervisors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Supervisors_SupervisorId",
                table: "Students");

            migrationBuilder.DropTable(
                name: "StudentNumbers");

            migrationBuilder.DropTable(
                name: "SupervisorSubjects");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.AlterColumn<int>(
                name: "SupervisorId",
                table: "Students",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Supervisors_SupervisorId",
                table: "Students",
                column: "SupervisorId",
                principalTable: "Supervisors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
