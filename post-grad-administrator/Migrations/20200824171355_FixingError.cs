﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace post_grad_administrator.Migrations
{
    public partial class FixingError : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StudentIdentificationNumber",
                table: "StudentNumbers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StudentIdentificationNumber",
                table: "StudentNumbers");
        }
    }
}
