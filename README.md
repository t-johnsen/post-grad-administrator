
# CRUD console application using .NET Core with Entity Framework 

## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Insert your credentials in DBContext class under OnConfiguring() method

Update with latest migration in Nuget console using the command ```update-database```

### Prerequisites

* .NET Framework

* Visual Studio 2017/19 OR Visual Studio Code

Packages:
* Microsoft.EntityFrameworkCore.SqlServer
* Microsoft.EntityFrameworkCore.Tools
* Newtonsoft.Json

### Project goals

The point of this project is to learn about relationships between entities and do CRUD operations using EF Core. Because of that there will be comments about the relationships in each class.
* _one to many_
* _one to one_
* _many to many_


[//]: #
[**]: <**>